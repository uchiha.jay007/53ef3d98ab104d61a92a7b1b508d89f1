import React from 'react';
import Items from './components/Items'
import ConfettiEffect from './components/Confetti';

const App = () => {
 
  return (
    <div className='app'>
       
      <Items /> 
     <ConfettiEffect></ConfettiEffect>
    </div>
  );
};

export default App;
